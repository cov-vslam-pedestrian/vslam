#include "lib/feature_tracking_detection.h"

#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#define MAX_FRAME 1000
#define MIN_NUM_FEAT 2000

int singh();
double getAbsoluteScale(int frame_id, int sequence_id, double z_cal);

int main() {
    std::cout << "Hello, World!" << std::endl;

    singh();
    return 0;
}

auto singh() -> int {
    const std::string imagePath = "/home/dominik/Downloads/2011_09_26/2011_09_26_drive_0095_extract/image_02/data/%010d.png";
    cv::Mat img_1, img_2;
    cv::Mat R_f, t_f; //the final rotation and tranlation vectors containing the

    std::ofstream myfile;
    myfile.open("results1_1.txt");

    double scale = 1.00;
    char filename1[200];
    char filename2[200];
    sprintf(filename1, "/home/dominik/Downloads/2011_09_26/2011_09_26_drive_0095_extract/image_02/data/%010d.png", 0);
    sprintf(filename2, "/home/dominik/Downloads/2011_09_26/2011_09_26_drive_0095_extract/image_02/data/%010d.png", 1);

    char text[100];
    int fontFace = cv::FONT_HERSHEY_PLAIN;
    double fontScale = 1;
    int thickness = 1;
    cv::Point textOrg(10, 50);

    //read the first two frames from the dataset
    cv::Mat img_1_c = cv::imread(filename1);
    cv::Mat img_2_c = cv::imread(filename2);

    if (!img_1_c.data || !img_2_c.data) {
        std::cout << " --(!) Error reading images " << std::endl;
        return -1;
    }

    // TODO INIT should be in Loop if possible!
    // Convert to gray scale color space
    cvtColor(img_1_c, img_1, cv::COLOR_BGR2GRAY);
    cvtColor(img_2_c, img_2, cv::COLOR_BGR2GRAY);

    // feature detection, tracking
    std::vector <cv::Point2f> points1, points2;        //vectors to store the coordinates of the feature points
    featureDetection(img_1, points1);        //detect features in img_1
    std::vector <uchar> status;
    featureTracking(img_1, img_2, points1, points2, status); //track those features to img_2

    //TODO: add a fucntion to load these values directly from KITTI's calib files
    // WARNING: different sequences in the KITTI VO dataset have different intrinsic/extrinsic parameters
    //double focal = 718.8560;
    //cv::Point2d pp(607.1928, 185.2157);
    cv::Mat cameraMatrix = (cv::Mat_<double>(3,3) << 959.7910, 0, 696.0217, 0, 956.9251, 224.1806, 0, 0, 1);


    //recovering the pose and the essential matrix
    cv::Mat E, R, t, mask;
    E = findEssentialMat(points2, points1, cameraMatrix, cv::RANSAC, 0.999, 1.0, mask);
    recoverPose(E, points2, points1, cameraMatrix, R, t,  mask);

    cv::Mat prevImage = img_2;
    cv::Mat currImage;
    std::vector <cv::Point2f> prevFeatures = points2;
    std::vector <cv::Point2f> currFeatures;

    char filename[100];

    R_f = R.clone();
    t_f = t.clone();

    clock_t begin = clock();

    // this should show the camera itself
    namedWindow("Road facing camera", cv::WINDOW_AUTOSIZE);

    // this should paint the trajectory of the person
    namedWindow("Trajectory", cv::WINDOW_AUTOSIZE);

    cv::Mat traj = cv::Mat::zeros(600, 600, CV_8UC3);

    // do the algorithm until MAX_FRAME
    for (int numFrame = 2; numFrame < MAX_FRAME; ++numFrame) {
        // TODO: Get next gray scale frame
        sprintf(filename, "/home/dominik/Downloads/2011_09_26/2011_09_26_drive_0095_extract/image_02/data/%010d.png", numFrame);
        cv::Mat currImage_c = cv::imread(filename);

        // break if we do not find a image
        if(currImage_c.empty()) {
            break;
        }

        cvtColor(currImage_c, currImage, cv::COLOR_BGR2GRAY);

        std::vector <uchar> status{};
        featureTracking(prevImage, currImage, prevFeatures, currFeatures, status);

        E = findEssentialMat(currFeatures, prevFeatures, cameraMatrix, cv::RANSAC, 0.999, 1.0, mask);
        recoverPose(E, currFeatures, prevFeatures, cameraMatrix, R, t, mask);

        // TODO: feature magic
        cv::Mat prevPts(2, prevFeatures.size(), CV_64F);
        cv::Mat currPts(2, currFeatures.size(), CV_64F);
        //std::cout << currPts << std::endl;
        for (int i = 0; i < prevFeatures.size(); i++) {   //this (x,y) combination makes sense as observed from the source code of triangulatePoints on GitHub
            prevPts.at<double>(0, i) = prevFeatures.at(i).x;
            prevPts.at<double>(1, i) = prevFeatures.at(i).y;

            currPts.at<double>(0, i) = currFeatures.at(i).x;
            currPts.at<double>(1, i) = currFeatures.at(i).y;
        }

        // determine scale
        //scale = getAbsoluteScale(numFrame, 0, t.at<double>(2));
        //cout << "Scale is " << scale << endl;

        /*if ((scale > 0.1) && (t.at<double>(2) > t.at<double>(0)) && (t.at<double>(2) > t.at<double>(1))) {

            //t_f = t_f + scale * (R_f * t);
            t_f = t_f + (R_f * t);
            R_f = R * R_f;

        } else {
            std::cout << "scale below 0.1, or incorrect translation" << std::endl;
        }*/
        t_f = t_f + (R_f * t);
        R_f = R * R_f;

        // lines for printing results
        // myfile << t_f.at<double>(0) << " " << t_f.at<double>(1) << " " << t_f.at<double>(2) << endl;

        // Can happen if features are out of the picture
        // a redetection is triggered in case the number of features being tracked go below a particular threshold
        if (prevFeatures.size() < MIN_NUM_FEAT) {
            //cout << "Number of tracked features reduced to " << prevFeatures.size() << endl;
            //cout << "trigerring redection" << endl;
            featureDetection(prevImage, prevFeatures);
            featureTracking(prevImage, currImage, prevFeatures, currFeatures, status);

        }

        // restart loop
        prevImage = currImage.clone();
        prevFeatures = currFeatures;

        // draw trajectory
        int x = int(t_f.at<double>(0)) + 300;
        int y = int(t_f.at<double>(2)) + 100;
        circle(traj, cv::Point(x, y), 1, CV_RGB(255, 0, 0), 2);

        rectangle(traj, cv::Point(10, 30), cv::Point(550, 50), CV_RGB(0, 0, 0), cv::FILLED);
        sprintf(text, "Coordinates: x = %02fm y = %02fm z = %02fm", t_f.at<double>(0), t_f.at<double>(1),
                t_f.at<double>(2));
        putText(traj, text, textOrg, fontFace, fontScale, cv::Scalar::all(255), thickness, 8);


        // draw circles around currently tracked features
        for (auto & featurePoint : currFeatures) {
            circle(currImage_c, featurePoint, 3, CV_RGB(100,0,0), 1,8,0);
        }


        cv::imshow("Road facing camera", currImage_c);
        cv::imshow("Trajectory", traj);

        cv::waitKey(1);

    }

    // output and time at the end
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "Total time taken: " << elapsed_secs << "s" << std::endl;

    std::cout << R_f << std::endl;
    std::cout << t_f << std::endl;

    return 0;
}

auto getAbsoluteScale(int frame_id, int sequence_id, double z_cal) -> double {

    std::string line;
    int i = 0;
    std::ifstream myfile("/home/dominik/Downloads/2011_09_26/2011_09_26_drive_0095_extract/image_02/data/00.txt");
    double x = 0, y = 0, z = 0;
    double x_prev, y_prev, z_prev;
    if (myfile.is_open()) {
        while ((getline(myfile, line)) && (i <= frame_id)) {
            z_prev = z;
            x_prev = x;
            y_prev = y;
            std::istringstream in(line);
            //cout << line << '\n';
            for (int j = 0; j < 12; j++) {
                in >> z;
                if (j == 7) y = z;
                if (j == 3) x = z;
            }

            i++;
        }
        myfile.close();
    } else {
        std::cout << "Unable to open file";
        return 0;
    }

    return sqrt((x - x_prev) * (x - x_prev) + (y - y_prev) * (y - y_prev) + (z - z_prev) * (z - z_prev));

}