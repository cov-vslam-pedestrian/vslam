# Visual Odemetry with SLAM (Simultaneous localization and mapping)
---

# Import Sources
Overview: [NCBI](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5084145/)

1. [Test Data - Vision TURN](https://vision.in.tum.de/data/datasets/rgbd-dataset/download)
2. [ORB Paper](http://www.willowgarage.com/sites/default/files/orb_final.pdf)
2. [ORB-SLAM](https://github.com/raulmur/ORB_SLAM2)
3. [ORB Intro](https://medium.com/@deepanshut041/introduction-to-orb-oriented-fast-and-rotated-brief-4220e8ec40cf)
4. [ORB vs AKAZE](https://docs.opencv.org/3.0-beta/doc/tutorials/features2d/akaze_tracking/akaze_tracking.html)